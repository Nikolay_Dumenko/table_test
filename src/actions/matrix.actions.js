import { bindActionCreators } from 'redux';
import { matrix as types } from 'constants';
import makeAction from 'utils/makeAction';


export const setX = makeAction(types.SET_X, 'x');

export const initMatrix = makeAction(types.INIT_MATRIX, 'data');
export const incrementCell = makeAction(types.INCREMENT_CELL, 'id');
export const addRow = makeAction(types.ADD_ROW, 'row');
export const deleteRow = makeAction(types.DELETE_ROW, 'row');

export function containerActions(dispatch) {
  return bindActionCreators({
    setX,
    initMatrix,
    incrementCell,
    addRow,
    deleteRow,
  }, dispatch);
}
