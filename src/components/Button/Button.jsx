import React from 'react';
import PropTypes from 'prop-types';
import pure from 'recompose/pure';


const propTypes = {
  disabled: PropTypes.bool,
  children: PropTypes.any.isRequired,
  onClick: PropTypes.func.isRequired,
};

const defaultProps = {
  disabled: false,
};

function Button({
  children,
  disabled,
  onClick,
}) {
  return (
    <button
      type="button"
      className="ui-button"
      onClick={onClick}
      disabled={disabled}
    >
      { children }
    </button>
  );
}

Button.propTypes = propTypes;
Button.defaultProps = defaultProps;

export default pure(Button);
