import React from 'react';
import PropTypes from 'prop-types';
import pure from 'recompose/pure';


const propTypes = {
  label: PropTypes.string.isRequired,
  placeholder: PropTypes.string.isRequired,
  value: PropTypes.number.isRequired,
  onChange: PropTypes.func.isRequired,
};

function Input({
  label,
  placeholder,
  value,
  onChange,
}) {
  function isValidValue(n) {
    return Number.isInteger(n) && n <= 1000;
  }

  function handleChange(evt) {
    const { value: v } = evt.target;
    const number = Number.parseInt(v, 10) || 0;

    if (!isValidValue(number)) return null;

    return onChange(number);
  }

  return (
    <label className="ui-field">
      <h6 className="ui-field-title">{label}</h6>
      <input
        className="ui-field-input"
        type="text"
        value={value}
        placeholder={placeholder}
        onChange={handleChange}
      />
    </label>
  );
}

Input.propTypes = propTypes;

export default pure(Input);
