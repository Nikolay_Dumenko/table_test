import React from 'react';
import PropTypes from 'prop-types';
import pure from 'recompose/pure';


const propTypes = {
  data: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
  amount: PropTypes.number.isRequired,
  isImmediate: PropTypes.any.isRequired,
  isPercent: PropTypes.bool.isRequired,
  id: PropTypes.number.isRequired,
};

function Cell({
  data,
  amount,
  isImmediate,
  isPercent,
  id,
}) {
  return (
    <td
      data-id={id}
      data-amount={amount}
      className={isImmediate || isPercent ? 'td-yellow' : ''}
    >
      {data}
    </td>
  );
}

Cell.propTypes = propTypes;

export default pure(Cell);
