import React from 'react';
import PropTypes from 'prop-types';
import getPercentOfRow from 'utils/getPercentOfRow';
import Cell from './Cell';


class Row extends React.Component {
  static propTypes = {
    data: PropTypes.array.isRequired,
    number: PropTypes.number.isRequired,
    immediate: PropTypes.oneOfType([PropTypes.array, PropTypes.bool]).isRequired,
    percent: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]).isRequired,
    onDeleteRow: PropTypes.func.isRequired,
    onMouseEnterPercent: PropTypes.func.isRequired,
    onMouseLeavePercent: PropTypes.func.isRequired,
  };

  shouldComponentUpdate(nextProps) {
    const {
      data,
      immediate,
      percent,
    } = this.props;
    return nextProps.data !== data
      || nextProps.immediate !== immediate
      || (nextProps.percent !== percent);
  }

  renderCols() {
    const {
      data,
      number,
      immediate,
      percent,
      onMouseEnterPercent,
      onMouseLeavePercent,
    } = this.props;
    const columns = [];
    const isPercent = percent && percent.is && percent.row === number;
    const getValue = isPercent && getPercentOfRow(data);
    let sum = 0;

    data.forEach((item) => {
      const { id } = item;
      const value = isPercent ? `${getValue(item.amount)}%` : item.amount;
      sum += item.amount;

      columns.push((
        <Cell
          key={id}
          isImmediate={immediate && immediate.some(el => item.amount === el)}
          isPercent={isPercent}
          id={item.id}
          amount={item.amount}
          data={value}
        />
      ));
    });

    return columns.concat((
      <td
        key={-1}
        onMouseEnter={onMouseEnterPercent(number)}
        onMouseLeave={onMouseLeavePercent}
      >
        { sum }
      </td>
    ));
  }

  render() {
    const { number, onDeleteRow } = this.props;
    return (
      <tr>
        <td>{ number + 1 }</td>
        { this.renderCols() }
        <td>
          <i className="row-delete" onClick={onDeleteRow(number)}>
            <img src="../images/trash.png" alt="delete" />
          </i>
        </td>
      </tr>
    );
  }
}

export default Row;
