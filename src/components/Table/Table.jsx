import React, { Component } from 'react';
import PropTypes from 'prop-types';

import TableHeader from './TableHeader';
import TableBody from './TableBody';
import TableFooter from './TableFooter';


class Table extends Component {
  static propTypes = {
    grid: PropTypes.array.isRequired,
    colsSum: PropTypes.array.isRequired,
    cols: PropTypes.number.isRequired,
    immediate: PropTypes.object.isRequired,
    percent: PropTypes.object.isRequired,
    onIncrementCell: PropTypes.func.isRequired,
    onDeleteRow: PropTypes.func.isRequired,
    onMouseMoveImmediate: PropTypes.func.isRequired,
    onMouseLeaveImmediate: PropTypes.func.isRequired,
    onMouseEnterPercent: PropTypes.func.isRequired,
    onMouseLeavePercent: PropTypes.func.isRequired,
  };

  shouldComponentUpdate(nextProps) {
    const { grid, percent, immediate } = this.props;
    return nextProps.grid !== grid
      || nextProps.immediate !== immediate
      || nextProps.percent !== percent;
  }

  render() {
    const {
      colsSum,
      cols,
      grid,
      immediate,
      percent,
      onIncrementCell,
      onDeleteRow,
      onMouseMoveImmediate,
      onMouseLeaveImmediate,
      onMouseEnterPercent,
      onMouseLeavePercent,
    } = this.props;

    return (
      <table
        className="table"
        onClick={onIncrementCell}
        onMouseMove={onMouseMoveImmediate}
        onMouseLeave={onMouseLeaveImmediate}
      >
        <TableHeader cells={colsSum.length} />
        <TableBody
          grid={grid}
          immediate={immediate}
          percent={percent}
          onDeleteRow={onDeleteRow}
          onMouseEnterPercent={onMouseEnterPercent}
          onMouseLeavePercent={onMouseLeavePercent}
        />
        <TableFooter colsSum={colsSum} cols={cols} />
      </table>
    );
  }
}

export default Table;
