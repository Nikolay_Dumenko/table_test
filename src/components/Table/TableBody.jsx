import React from 'react';
import PropTypes from 'prop-types';
import Row from './Row';


const propTypes = {
  grid: PropTypes.array.isRequired,
  immediate: PropTypes.object.isRequired,
  percent: PropTypes.object.isRequired,
  onDeleteRow: PropTypes.func.isRequired,
  onMouseEnterPercent: PropTypes.func.isRequired,
  onMouseLeavePercent: PropTypes.func.isRequired,
};

function TableBody({
  grid,
  immediate,
  percent,
  onDeleteRow,
  onMouseEnterPercent,
  onMouseLeavePercent,
}) {
  function getImmediate(i) {
    if (!immediate.is || !immediate.numbers[i]) return false;
    return immediate.numbers[i];
  }

  function renderRows() {
    return grid.map((item, i) => (
      <Row
        key={i}
        number={i}
        data={item}
        immediate={getImmediate(i)}
        percent={percent.is && percent.row === i && percent}
        onDeleteRow={onDeleteRow}
        onMouseEnterPercent={onMouseEnterPercent}
        onMouseLeavePercent={onMouseLeavePercent}
      />
    ));
  }

  return (
    <tbody className="table-body">
      { renderRows() }
    </tbody>
  );
}

TableBody.propTypes = propTypes;

export default TableBody;
