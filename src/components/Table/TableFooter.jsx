import React from 'react';
import PropTypes from 'prop-types';
import pure from 'recompose/pure';


const propTypes = {
  cols: PropTypes.number.isRequired,
  colsSum: PropTypes.array.isRequired,
};

function TableFooter({
  colsSum,
  cols,
}) {
  function renderRow() {
    return colsSum.map((item, i) => (
      <td key={i}>{(item / cols).toFixed(2)}</td>
    ));
  }

  return (
    <tfoot className="table-footer">
      <tr>
        <td>mean:</td>
        { renderRow() }
        <td colSpan="2" />
      </tr>
    </tfoot>
  );
}

TableFooter.propTypes = propTypes;

export default pure(TableFooter);
