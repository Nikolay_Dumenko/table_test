import React from 'react';
import PropTypes from 'prop-types';
import pure from 'recompose/pure';


const propTypes = {
  cells: PropTypes.number.isRequired,
};

function TableHeader({ cells }) {
  function renderRow() {
    const row = [];

    for (let i = 0; i < cells; i += 1) {
      row.push(<th key={i}>{i + 1}</th>);
    }

    return row;
  }

  return (
    <thead className="table-header">
      <tr>
        <th>#</th>
        { renderRow() }
        <th>Sum</th>
        <th>Delete</th>
      </tr>
    </thead>
  );
}

TableHeader.propTypes = propTypes;

export default pure(TableHeader);
