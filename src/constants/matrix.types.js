const prefix = 'matrix';

export const SET_X = `${prefix}/SET_X`;

export const INIT_MATRIX = `${prefix}/INIT_MATRIX`;
export const INCREMENT_CELL = `${prefix}/INCREMENT_CELL`;
export const ADD_ROW = `${prefix}/ADD_ROW`;
export const DELETE_ROW = `${prefix}/DELETE_ROW`;
