import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import { containerActions } from 'actions/matrix.actions';
import generateMatrix from 'utils/generateMatrix';
import Input from 'components/Input';
import Button from 'components/Button';


const style = {
  form: {
    display: 'flex',
    alignItems: 'flex-end',
    justifyContent: 'center',
    paddingBottom: '20px',
  },
};

class Form extends Component {
  static propTypes = {
    count: PropTypes.number.isRequired,
    setX: PropTypes.func.isRequired,
    initMatrix: PropTypes.func.isRequired,
  };

  state = {
    cols: 10,
    rows: 1000,
  };

  isDisabled() {
    const { cols, rows } = this.state;
    return !cols || !rows;
  }

  handleChangeField = name => value => this.setState({ [name]: value });

  handleChangeCount = (value) => {
    const { setX } = this.props;
    setX(value);
  };

  handleSubmit = () => {
    const { initMatrix } = this.props;
    const { cols, rows } = this.state;
    const matrix = generateMatrix(rows, cols);

    initMatrix(matrix);
  };

  render() {
    const { count } = this.props;
    const { cols, rows } = this.state;

    return (
      <div style={style.form}>
        <Input
          label="cols"
          placeholder="Enter cols"
          value={cols}
          onChange={this.handleChangeField('cols')}
        />

        <Input
          label="rows"
          placeholder="Enter rows"
          value={rows}
          onChange={this.handleChangeField('rows')}
        />

        <Input
          label="count X"
          placeholder="Enter X"
          value={count}
          onChange={this.handleChangeCount}
        />

        <Button
          onClick={this.handleSubmit}
          disabled={this.isDisabled()}
        >
          generate
        </Button>
      </div>
    );
  }
}

const selector = createStructuredSelector({
  count: state => state.matrix.count,
});


export default connect(selector, containerActions)(Form);
