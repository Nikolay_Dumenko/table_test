import React from 'react';

import Form from 'containers/Form';
import Table from './Table';


const style = { margin: '30px auto', width: '1200px' };

function Root() {
  return (
    <div style={style}>
      <Form />
      <Table />
    </div>
  );
}

export default Root;
