import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';

import { containerActions } from 'actions/matrix.actions';
import generateMatrix from 'utils/generateMatrix';
import getClosets from 'utils/getClosets';
import Table from 'components/Table';
import Button from 'components/Button';


const style = {
  padding: '20px 0 10px',
  display: 'flex',
  justifyContent: 'flex-end',
};

class TableContainer extends Component {
  static propTypes = {
    data: PropTypes.array.isRequired,
    matrix: PropTypes.array.isRequired,
    colsSum: PropTypes.array.isRequired,
    count: PropTypes.number.isRequired,
    rows: PropTypes.number.isRequired,
    cols: PropTypes.number.isRequired,
    incrementCell: PropTypes.func.isRequired,
    addRow: PropTypes.func.isRequired,
    deleteRow: PropTypes.func.isRequired,
  };

  state = {
    percent: {
      is: false,
      row: 0,
    },
    immediate: {
      is: false,
      value: 0,
      numbers: [],
    },
  };

  // handlers
  handleIncrementCell = (evt) => {
    const { incrementCell } = this.props;
    const { target } = evt;

    if (target.hasAttribute('data-id')) {
      const id = Number(target.getAttribute('data-id'));
      this.setState({
        immediate: {
          is: false,
          value: 0,
          numbers: [],
        },
      });
      incrementCell(id);
    }
  }

  handleAddRow = () => {
    const { addRow, rows } = this.props;
    const row = generateMatrix(1, rows);
    addRow(...row);
  }

  handleDeleteRow = row => () => {
    const { deleteRow } = this.props;
    deleteRow(row);
  };

  handleMouseMoveImmediate = (evt) => {
    const { data, count } = this.props;
    const { immediate } = this.state;
    const { target } = evt;

    if (target.hasAttribute('data-amount') && (count > 0)) {
      const amount = Number(target.getAttribute('data-amount'));
      if (amount !== immediate.value) {
        this.setState({
          immediate: {
            is: true,
            value: amount,
            numbers: getClosets(data, amount, count),
          },
        });
      }
    } else if (immediate.is) {
      this.setState({
        immediate: {
          is: false,
          value: 0,
          numbers: [],
        },
      });
    }
  };

  handleMouseLeaveImmediate = () => {
    this.setState({
      immediate: {
        is: false,
        value: 0,
      },
    });
  };

  handleEnterShowPercent = row => () => {
    this.setState({
      percent: {
        is: true,
        row,
      },
    });
  };

  handleLeaveHidePercent = () => {
    this.setState({
      percent: {
        is: false,
        row: 0,
      },
    });
  };


  render() {
    const { matrix, cols, colsSum } = this.props;
    const { immediate, percent } = this.state;

    if (!matrix.length) return null;

    return (
      <Fragment>
        <div style={style}>
          <Button
            onClick={this.handleAddRow}
          >
            add row
          </Button>
        </div>
        <Table
          grid={matrix}
          immediate={immediate}
          percent={percent}
          colsSum={colsSum}
          cols={cols}
          onIncrementCell={this.handleIncrementCell}
          onDeleteRow={this.handleDeleteRow}
          onMouseMoveImmediate={this.handleMouseMoveImmediate}
          onMouseLeaveImmediate={this.handleMouseLeaveImmediate}
          onMouseEnterPercent={this.handleEnterShowPercent}
          onMouseLeavePercent={this.handleLeaveHidePercent}
        />
      </Fragment>
    );
  }
}

const selector = createStructuredSelector({
  matrix: state => state.matrix.data,
  colsSum: state => state.matrix.colsSum,
  rows: state => (state.matrix.data[0] ? state.matrix.data[0].length : 0),
  cols: state => state.matrix.data.length,
  data: state => state.matrix.data.reduce((arr, curr) => {
    arr.push(...curr);
    return arr;
  }, []),
  count: state => state.matrix.count,
});


export default connect(selector, containerActions)(TableContainer);
