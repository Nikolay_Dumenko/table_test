import React from 'react';
import { render } from 'react-dom';
import { createStore, applyMiddleware, compose } from 'redux';
import { Provider } from 'react-redux';
import { createLogger } from 'redux-logger';
// import 'semantic-ui-css/semantic.min.css';

import Root from 'containers/Root';
import reducer from './reducers';


// store
const node = document.getElementById('root');
const middleware = [];
let store = null;


if (process.env.NODE_ENV === 'production') {
  store = createStore(reducer, applyMiddleware(...middleware));
} else {
  const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
  const logger = createLogger();

  middleware.push(logger);
  store = createStore(reducer, composeEnhancers(applyMiddleware(...middleware)));
}

function App() {
  return (
    <Provider store={store}>
      <Root />
    </Provider>
  );
}

render(<App />, node);
