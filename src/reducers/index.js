import { combineReducers } from 'redux';

// reducers
import matrix from './matrix.reducer';

export default combineReducers({
  matrix,
});
