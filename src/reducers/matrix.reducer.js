import { matrix as types } from 'constants';


const initialState = {
  data: [],
  colsSum: [],
  count: 0,
};

function createData(state, matrix) {
  let count = -1;
  const colsSum = [];
  const data = matrix.map((rows, i) => rows.map((cols, j) => {
    count += 1;
    colsSum[j] = (colsSum[j] || 0) + cols;
    return {
      id: count,
      row: i,
      amount: cols,
    };
  }));

  return { ...state, data, colsSum };
}

function incrementCell(state, id) {
  const { data: matrix, colsSum: mean } = state;
  const data = matrix.map(x => x.map((y, c) => {
    if (y.id === id) {
      mean[c] += 1;
      return { ...y, amount: y.amount + 1 };
    }

    return { ...y };
  }));
  const colsSum = [...mean];

  return { ...state, data, colsSum };
}

function addRow(state, row) {
  const { data: matrix, colsSum: mean } = state;
  let count = -1;
  const oldRows = matrix.map((rows, i) => rows.map((cols) => {
    count += 1;
    return {
      id: count,
      row: i,
      amount: cols.amount,
    };
  }));
  const newRow = row.map((cols, j) => {
    count += 1;
    mean[j] += cols;
    return {
      id: count,
      row: oldRows.length,
      amount: cols,
    };
  });

  return {
    ...state,
    colsSum: [...mean],
    data: [].concat(oldRows, [newRow]),
  };
}

function deleteRow(state, row) {
  const { data: matrix } = state;
  const colsSum = [];
  let count = -1;
  const data = matrix.reduce((arr, rows, index) => {
    if (row === index) return arr;

    const result = rows.map((cols, j) => {
      count += 1;
      colsSum[j] = (colsSum[j] || 0) + cols.amount;
      return {
        id: count,
        row: index,
        amount: cols.amount,
      };
    });

    return arr.concat([result]);
  }, []);

  return { ...state, colsSum, data };
}

export default function (state = initialState, action) {
  const { type, payload } = action;

  switch (type) {
    case types.SET_X:
      return { ...state, count: payload.x };

    case types.INIT_MATRIX:
      return createData(state, payload.data);

    case types.INCREMENT_CELL:
      return incrementCell(state, payload.id);

    case types.ADD_ROW:
      return addRow(state, payload.row);

    case types.DELETE_ROW:
      return deleteRow(state, payload.row);

    default:
      return state;
  }
}
