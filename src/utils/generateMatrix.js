import randomNumber from 'utils/randomNumber';


/**
 * @function generateMatrix
 * @param {number} rows
 * @param {number} cols
 * @returns {array}
 */
export default function (rows, cols) {
  const matrix = [];

  for (let i = 0; i < rows; i += 1) {
    matrix[i] = [];
    for (let j = 0; j < cols; j += 1) {
      matrix[i][j] = randomNumber(999);
    }
  }

  return matrix;
}
