export default function (data, value, count) {
  const closests = data.sort((a, b) => Math.abs(value - a.amount) - Math.abs(value - b.amount));
  const arr = closests.slice(0, count);
  const test = arr.reduce((obj, curr) => {
    obj[curr.row] = obj.hasOwnProperty(curr.row) ? [...obj[curr.row], curr.amount] : [curr.amount];
    return obj;
  }, {});

  return test;
}
