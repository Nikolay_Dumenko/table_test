/**
 * @function getPercentOfRow
 * @param {array} row
 * @return {function}
 */
export default function (row) {
  if (!Array.isArray(row)) throw new Error('row is not array');

  const sum = row.reduce((acc, current) => acc + current.amount, 0);

  return n => (n / sum * 100).toFixed(2);
}
