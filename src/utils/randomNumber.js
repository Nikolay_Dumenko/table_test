/**
 * @function randomNumber
 * @param {number} max
 * @return {number}
 */
export default function (max = 999) {
  return Math.floor(Math.random() * max);
}
